// Tooltips  //
let tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
let tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl);
});
// Popover //
let popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
let popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
  return new bootstrap.Popover(popoverTriggerEl);
});
// Carousel //
const myCarousel = document.querySelector('#carouselExampleIndicators');
let carousel = new bootstrap.Carousel(myCarousel, {
  interval: 2000,
  wrap: false
});

// Modal //
const miModal = document.querySelector('#exampleModal');
miModal.addEventListener('show.bs.modal', event => {
  console.log('Modal comienza a abrirse');
  const botones = document.getElementsByClassName('btn-outline-info');
  botones.disabled = true;
  console.log('Boton disabled');
});
miModal.addEventListener('shown.bs.modal', event => {
  console.log('Modal se abrio');
});
miModal.addEventListener('hide.bs.modal', event => {
  console.log('Modal comienza a cerrarse');
  const botones = document.getElementsByClassName('btn-outline-info');
  botones.disabled = false;
  console.log('Console Enabled');
});
miModal.addEventListener('hidden.bs.modal', event => {
  console.log('Modal se cerro');
});